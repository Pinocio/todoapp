<?php

namespace App\Form;

use App\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, [
                'label'=>'Имя'
            ])
            ->add('email', EmailType::class, [
                'label'=>'E-mail'
            ])
            ->add('text', null, [
                'label'=>'Текст задания'
            ])
            ->add('status', ChoiceType::class,[
                'choices'=>[
                    'Выполняется' => 0,
                    'Завершена'   => 1,
                    'Отклонена'   => 2
                ]
            ])
            ->add('submit', SubmitType::class,[
                'label'=>'Отправить'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}
