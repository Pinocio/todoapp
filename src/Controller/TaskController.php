<?php

namespace App\Controller;

use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Form\TaskFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class TaskController extends AbstractController
{

    private $twig;
    private $entityManager;
    private $requestStack;
    public function __construct(Environment $twig, EntityManagerInterface $entityManager, RequestStack $requestStack)
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
    }
    #[Route('/', name: 'homepage')]
    public function index(Request $request, TaskRepository $taskRepository, NotifierInterface $notifier): Response
    {
        
        $task  = new Task();
        $form = $this->createForm(TaskFormType::class, $task);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid())
        {
            $task->setTask($task);
            $this->entityManager->persist($task);
            $this->entityManager->flush();
            $notifier->send(new Notification('Ваша задача была успешно добавлена! Спасибо.',['browser']));
            //return $this->redirectToRoute('homepage');
        }
        $session = $this->requestStack->getSession();
        if($request->get('sort') !== null){
            $this->sort = $request->get('sort');
            $session->set('sort',  $this->sort);
        }
        elseif ($session->get('sort') !== null){
            $this->sort =$session->get('sort');
            $session->set('sort',  $this->sort);
        }else{
            $this->sort = 'id';
            $session->set('sort',  $this->sort);
        }
        if($request->get('order') !== null){
            $this->order = $request->get('order');
            $session->set('order',   $this->order);
        }
        elseif ($session->get('order') !== null){
            $this->order = $session->get('order');
            $session->set('order',   $this->order);
        }else{
            $this->order = 'ASC';
            $session->set('order',   $this->order);
        }
        $page = $request->get('page', 1);
        return new Response($this->twig->render('task/index.html.twig',[
            'controller_name'=>'TaskController',
            'task_form' => $form->createView(),
            'tasks'=>$taskRepository->getPagination($page, $this->sort, $this->order),
        ]));
    }
    #[Route('/edit/{id}', name: 'edit')]
    public function editTask(int $id, Request $request, TaskRepository $taskRepository, NotifierInterface $notifier):Response
    {
        $task = $taskRepository->find($id);
        $form = $this->createForm(TaskFormType::class, $task);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $task->setTask($task);
            $this->entityManager->persist($task);
            $this->entityManager->flush();
            $notifier->send(new Notification('Ваша задача была успешно отредактирована! Спасибо.',['browser']));
            return $this->redirectToRoute('homepage');
        }
        $session = $this->requestStack->getSession();
        if($request->get('sort') !== null){
            $this->sort = $request->get('sort');
            $session->set('sort',  $this->sort);
        }
        elseif ($session->get('sort') !== null){
            $this->sort =$session->get('sort');
            $session->set('sort',  $this->sort);
        }else{
            $this->sort = 'id';
            $session->set('sort',  $this->sort);
        }
        if($request->get('order') !== null){
            $this->order = $request->get('order');
            $session->set('order',   $this->order);
        }
        elseif ($session->get('order') !== null){
            $this->order = $session->get('order');
            $session->set('order',   $this->order);
        }else{
            $this->order = 'ASC';
            $session->set('order',   $this->order);
        }
        $page = $request->get('page', 1);
        return new Response($this->twig->render('task/index.html.twig',[
            'controller_name'=>'TaskController',
            'task_form' => $form->createView(),
            'tasks'=>$taskRepository->getPagination($page, $this->sort, $this->order),
        ]));
    }
    #[Route('/delete/{id}', name: 'delete')]
    public function deleteTask(int $id, TaskRepository $taskRepository){
        $task = $taskRepository->find($id);
        $this->entityManager->remove($task);
        $this->entityManager->flush();
        return $this->redirectToRoute('homepage');
    }
}
