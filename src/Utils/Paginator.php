<?php
namespace App\Utils;

use ArrayIterator;
use Doctrine\ORM\QueryBuilder;
// use Doctrine\ORM\Tools\Pagination\Paginator as PaginatorClass;

class Paginator
{
    private const PAGE_SIZE = 3;
    private ArrayIterator $result;
    private int $numResult;
    private int $currentPage;
    
    public function __construct(
        private QueryBuilder $queryBuilder,
        private int $pageSize = self::PAGE_SIZE,
    )
    {}

    public function pagination(int $page = 1):self
    {
        $this->currentPage = (int)max(1, $page);
        $firstResult = ($this->currentPage - 1) * $this->pageSize;
        $query = $this->queryBuilder
            ->setFirstResult($firstResult)
            ->setMaxResults($this->pageSize)
            ->getQuery();
        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query, true);
        $this->result = $paginator->getIterator();
        $this->numResult = $paginator->count();
        return $this;
    }

    public function getNumResult()
    {
        return $this->numResult;
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getLastPage(): int
    {
        return (int)ceil($this->numResult / $this->pageSize);
    }
    
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    public function hasPrevPage():bool
    {
        return $this->currentPage > 1;
    }

    public function getPrevPage():int
    {
        return max(1, $this->currentPage - 1);
    }

    public function hasNextPage():bool
    {
        return $this->currentPage < $this->getLastPage();
    }

    public function getNextPage():int
    {
        return min($this->getLastPage(), $this->currentPage + 1);
    }

    public function hasToPaginate(): bool
    {
        return $this->numResult > $this->pageSize;
    }
}