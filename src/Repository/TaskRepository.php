<?php

namespace App\Repository;

use App\Entity\Task;
use App\Utils\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }
    public function getPagination(int $page, string $sort, string $order):Paginator
    {
        $qb = $this->createQueryBuilder('t')
            ->orderBy('t.'.$sort, $order);
        return (new Paginator($qb))->pagination($page);
    }
}
